# Overview

This Django application is to allow use of Ordnance Survey's
[Code-Point
Open](http://www.ordnancesurvey.co.uk/oswebsite/products/code-point-open/)
data.

Currently it requires [MySociety's version of the
data](http://parlvid.mysociety.org:81/os/), but that might change in
the future.

Due to the slowness of loading fixtures in Django the script to
convert the data outputs SQL statements rather than JSON.  This SQL is
currently only in PostgreSQL dialect.
