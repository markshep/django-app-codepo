#!/usr/bin/python -ttu


import glob
import os
import pg
import sys


#-----------------------------------------------------------------------------#


def load_codes(path):
    counties = []
    districts = {}
    wards = []
    fh = open(path)
    while True:
        line = fh.readline()
        if len(line) == 0:
            break
        bits = line.rstrip(" \r\n").split(",", 3)
        if (len(bits[0]) != 2) or not bits[0].isdigit():
            continue
        assert(len(bits) == 4)

        county_id = int(bits[0])
        name = bits[3]
        if bits[1] == "":
            counties.append((county_id, name,))
        elif bits[2] == "":
            if name.endswith(" (B)"):
                # dunno what this means, so strip it off!
                name = name[: -4]
            if name.endswith(" Boro"):
                # they've truncated these!?!
                name += "ugh"
            code = bits[0] + bits[1]
            districts[code] = (len(districts) + 1, name, code, county_id or None,)
        else:
            code = bits[0] + bits[1] + bits[2]
            district_id = districts[bits[0] + bits[1]][0]
            wards.append((len(wards) + 1, name, code, district_id,))
    fh.close()
    districts = sorted(districts.values(), key=lambda a: a[0])
    return counties, districts, wards,


def add_counties(fh, counties):
    fh.write("COPY codepo_county (id, name) FROM stdin;\n")
    maxlen = 0
    for pk, name, in counties:
        fh.write("%d\t%s\n" % (pk, name,))
        maxlen = max(maxlen, len(name))
    fh.write("\\.\n")
    print "Wrote %d counties, name maxlen = %d" % (len(counties), maxlen,)


def add_districts(fh, districts):
    fh.write("COPY codepo_district (id, name, code, county_id) FROM stdin;\n")
    maxlen = 0
    for pk, name, code, county_id, in districts:
        if county_id is None:
            county_id = "\\N" # this escape means null
        fh.write("%d\t%s\t%s\t%s\n" % (pk, name, code, county_id,))
        maxlen = max(maxlen, len(name))
    fh.write("\\.\n")
    print "Wrote %d unitary authorities/districts, name maxlen = %d" % (pk, maxlen,)


def add_wards(fh, wards):
    fh.write("COPY codepo_ward (id, name, code, district_id) FROM stdin;\n")
    maxlen = 0
    for pk, name, code, district_id, in wards:
        fh.write("%d\t%s\t%s\t%d\n" % (pk, name, code, district_id,))
        maxlen = max(maxlen, len(name))
    fh.write("\\.\n")
    print "Wrote %d wards, name maxlen = %d" % (len(wards), maxlen,)


def add_areas(fh, codes):
    fh.write("COPY codepo_area (id, code) FROM stdin;\n")
    areas = {}
    for i, code in enumerate(codes):
        fh.write("%d\t%s\n" % (i, code,))
        areas[code] = i
    fh.write("\\.\n")
    print "Found %d postcode areas" % (i,)
    return areas


def add_postcodes(sql, areas, wards, csv_files):
    sql.write("COPY codepo_postcode (id, postcode, area_id, lat, lon, ward_id) FROM stdin;\n")
    skips = set()
    count = 0
    for path in csv_files:
        area = path2area(path)
        print "Doing %s..." % (area,)
        area_id = areas[area]
        fh = open(path)
        while True:
            line = fh.readline()
            if line == "":
                break
            fields = line.rstrip("\r\n").split(",")
            postcode = format_postcode(fields[0].strip("\""))
            lat = float(fields[2])
            lon = float(fields[3])
            ward_code = fields[7].strip("\"") + fields[8].strip("\"") + fields[9].strip("\"")
            if len(ward_code) == 0:
                skips.add(postcode)
                continue
            if ward_code.startswith("00HF"):
                # Isles of Scilly - parish names are missing from codelist.txt
                skips.add(postcode)
                continue
            ward_id = wards[ward_code]
            count += 1
            sql.write("%d\t%s\t%d\t%.20f\t%.20f\t%d\n" % (count, postcode, area_id, lat, lon, ward_id,))
        fh.close()
    sql.write("\\.\n")
    print "Skipped %d postcodes due to missing data" % (len(skips),)
    print "Added %d postcodes with valid location information" % (count,)


def path2area(path):
    return os.path.splitext(os.path.basename(path))[0].upper()


def format_postcode(orig):
    return "%s %s" % (orig[0:4].rstrip(" "), orig[4:],)


ward_ids = {}
def get_ward_id(code):
    if not code in ward_ids:
        result = db.query("SELECT * FROM codepo_ward WHERE code='%s'" % (db.escape_string(code),)).getresult()
        if len(result) == 0:
            sys.exit("Can't find ward with code \"%s\"" % (code,))
        ward_ids[code] = result[0][0]
    return ward_ids[code]


#-----------------------------------------------------------------------------#


proj_dir = os.path.normpath(__file__ + "/../..")
data_dir = proj_dir + "/codepo_gb_wgs84"
codes_path = data_dir + "/doc/codelist.txt"
counties, districts, wards, = load_codes(codes_path)

sql = open(proj_dir + "/codepo.sql", "w")
add_counties(sql, counties)
add_districts(sql, districts)
add_wards(sql, wards)

csv_dir = data_dir + "/data/CSV"
csv_files = sorted(glob.glob("%s/*.csv" % (csv_dir,)))
codes = map(path2area, csv_files)
areas = add_areas(sql, codes)
ward_ids = {}
for pk, name, code, district_id, in wards:
    ward_ids[code] = pk
add_postcodes(sql, areas, ward_ids, csv_files)
sql.close()

print
print "Useful commands to run from the project directory:"
print "  Destroy the existing tables:"
print "    ./manage.py sqlclear codepo | ./manage.py dbshell"
print "  Create the empty tables:"
print "    ./manage.py syncdb"
print "  Load the postcode data:"
print "    ./manage.py dbshell < codepo.sql"


#-----------------------------------------------------------------------------#
