from django.db import models
#from django.contrib.gis.db import models


class County(models.Model):
    name = models.CharField(max_length=32, unique=True)


class District(models.Model):
    name = models.CharField(max_length=64)
    code = models.CharField(max_length=4)
    # if null then this is a unitary authority
    county = models.ForeignKey(County, null=True)


class Ward(models.Model):
    name = models.CharField(max_length=64)
    code = models.CharField(max_length=6)
    district = models.ForeignKey(District)


class Area(models.Model):
    code = models.CharField(max_length=2, unique=True)


class Postcode(models.Model):
    postcode = models.CharField(max_length=8, unique=True)
    area = models.ForeignKey(Area)
    lat = models.FloatField(db_index=True)
    lon = models.FloatField(db_index=True)
    ward = models.ForeignKey(Ward)
